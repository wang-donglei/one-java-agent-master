package com.gientech.agent;

import io.oneagent.service.ClassLoaderHandler;

/*
 * 类加载处理类
 *@author wangdonglei
 *@create 2024/3/2 9:35
 */
public class ApolloPluginClassLoaderHandler implements ClassLoaderHandler {

    @Override
    public Class<?> loadClass(String name) {
        if (name.startsWith("com.gientech.agent")) {
            try {
                Class<?> clazz = this.getClass().getClassLoader().loadClass(name);
                return clazz;
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
