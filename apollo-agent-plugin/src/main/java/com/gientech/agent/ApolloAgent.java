package com.gientech.agent;

import com.ctrip.framework.apollo.ConfigChangeListener;
import com.ctrip.framework.apollo.ConfigService;
import com.ctrip.framework.apollo.model.ConfigChange;
import com.ctrip.framework.apollo.model.ConfigChangeEvent;

import java.lang.instrument.Instrumentation;

public class ApolloAgent {

    public static void premain(String args, Instrumentation inst) {
        init(true, args, inst);
    }

    public static void agentmain(String args, Instrumentation inst) {
        init(false, args, inst);
    }

    public static void init(boolean premain, String args, Instrumentation inst) {
        System.out.println("apollo agent start success!");

        // 手动配置 apolloConfigListener，添加配置改动监听
        ConfigChangeListener configChangeListener = new ConfigChangeListener() {
            @Override
            public void onChange(ConfigChangeEvent configChangeEvent) {
                ConfigChange change;
                for (String key : configChangeEvent.changedKeys()) {
                    change = configChangeEvent.getChange(key);
                    // 打印改动的配置信息
                    System.out.println(
                            String.format(String.format("Change - key: %s, oldValue: %s, newValue: %s, changeType: %s",
                                    change.getPropertyName(), change.getOldValue(), change.getNewValue(),
                                    change.getChangeType())));
                }
            }
        };

        ConfigService.getAppConfig().addChangeListener(configChangeListener);
    }
}
